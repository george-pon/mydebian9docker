#!/bin/bash
#
#  debian9 イメージ上で、sshdを開始する
#

set -e

mkdir -p /run/sshd

echo "start sshd daemon"
/usr/sbin/sshd -D &

echo "create users key pair."
ssh-keygen -t rsa -m PEM -N "" -f ~/.ssh/id_rsa

echo "set authorized_keys"
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

echo "SUCCESS."
